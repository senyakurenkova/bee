let basicLevel = document.getElementById('basic_level');
let elementsH2 = basicLevel.querySelectorAll('h2'); // все элементы этого блока div с тегом h2
						  
for (let elem of elementsH2) { // цикл по элементам с тегом h2
    elem.innerText = elem.innerText.toUpperCase(); // переприсваиваем элементам их же значения, но капсом
}

let elementsH3 = basicLevel.querySelectorAll('h3'); // все элементы этого блока div с тегом h3

for (let elem of elementsH3) {  // цикл по элементам с тегом h3
    if(elem.innerText.length > 20) // условие что длина содержимого текста элемента меньше 20 символов
        elem.innerText = elem.innerText.substr(0,20)+"..." //  инача присваиваем 20 первых символов и ...
}
