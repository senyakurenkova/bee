window.onload = function() {
    const elemLec =[...document.querySelectorAll(".blog")];
    const elemCountLec = document.getElementById("countLec");
    const allLectures = document.getElementById("allLectures");

    const countLec =  elemLec.length;
    elemCountLec.innerText = countLec + " лекций";
    
    for(let i=0; i<countLec; i++)
    {
        const cloneNode = elemLec[i].cloneNode(true);
        allLectures.appendChild(cloneNode);
    }

  };

  document.getElementById("countLec").parentElement.onclick = evt => {

        const btn = evt.path[0].innerText;
        if(!btn) return;

        const elemLec = evt.path[3].querySelectorAll(".blog");
        let dis = "none";

        if(evt.path[0].id === "countLec")
            dis = "";
        elemLec.forEach((element) => {

        element.style.display=dis;
        if(element.dataset.group == btn)
            element.style.display="";
        });
};
