window.onload = function() {
    const elemMenuUl = document.querySelector("ul#menu");
    const elemUl = elemMenuUl.querySelectorAll("ul");
    elemUl.forEach((element) => {
        element.style.display="none";
      });
  };

  document.querySelector("ul#menu").onclick = evt => {
    if(!evt.isTrusted || !evt.path[1].querySelector('ul')) 
        return; 

    if(evt.path[0].classList.contains('top-line--active'))
    {
        evt.path[0].classList.remove('top-line--active');
        evt.path[1].querySelector('ul').style.display="none";
    }
    else
    {
        evt.path[0].classList.add('top-line--active');
        evt.path[1].querySelector('ul').style.display="";
    } 
}