$(document).ready(function(){
  initMenu();
});

function initMenu() {

  $('#menu ul').hide();
$('#menu li a').click(
  function() {
    var iselemnt = $(this).next();
    if(!iselemnt.is('ul')){
      return;
    }
    if(iselemnt.is(':visible')) {
      iselemnt.slideUp('normal');
      if($(this).hasClass('top-line--active'))
      $(this).removeClass('top-line--active');
    }
    else{
      $('#menu ul:visible').slideUp('normal');
      iselemnt.slideDown('normal');
      $(this).toggleClass('top-line--active');
    }

  }
);

}
